#!/usr/bin/env bash

set -x

# set -Eeuo pipefail

if [ -e /usr/bin/mongodb_exporter ]
then
  /usr/bin/mongodb_exporter \
    --log.level="info" \
    --mongodb.uri=mongodb://monitoring:monitoring@127.0.0.1:27017/admin?authSource=admin \
    --no-collector.replicasetstatus \
    --discovering-mode \
    --web.listen-address=:${MONGODB_EXPORTER_PORT:-9216} &

#       --mongodb.collstats-colls=db1.col1,db2.col2                         List of comma separared databases.collections to get $collStats
#       --mongodb.indexstats-colls=db1.col1,db2.col2                        List of comma separared databases.collections to get $indexStats
#       --mongodb.uri=mongodb://user:pass@127.0.0.1:27017/admin?ssl=true    MongoDB connection URI ($MONGODB_URI)
#       --mongodb.global-conn-pool                                          Use global connection pool instead of creating new pool for each http request.
#       --mongodb.direct-connect                                            Whether or not a direct connect should be made. Direct connections are not valid if multiple hosts are specified or an SRV URI is used.
#       --web.listen-address=":9216"                                        Address to listen on for web interface and telemetry
#       --web.telemetry-path="/metrics"                                     Metrics expose path
#       --log.level="error"                                                 Only log messages with the given severuty or above. Valid levels: [debug, info, warn, error, fatal]
#       --no-collector.diagnosticdata                                       Disable collecting metrics from getDiagnosticData
#       --no-collector.replicasetstatus                                     Disable collecting metrics from replSetGetStatus
#       --collector.dbstats                                                 Enable collecting metrics from dbStats
#       --collector.collstats-limit=0                                       Enable collstats and indexstats collector only if there are less than <n> collections. -1=No limit
#       --collector.topmetrics                                              Enable collecting metrics from top admin command
#       --discovering-mode                                                  Enable autodiscover collections
#       --compatible-mode                                                   Enable old mongodb-exporter compatible metrics
#       --version                                                           Show version and exit
fi

/usr/local/bin/docker-entrypoint.sh "$@"
