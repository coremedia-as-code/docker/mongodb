#!/usr/bin/env bash

# set -x
set -e

# mongo admin \
#   --host localhost \
#   --username ${MONGO_INITDB_ROOT_USERNAME} \
#   --password ${MONGO_INITDB_ROOT_PASSWORD} \
#   --eval "db.getLogComponents();"

# see:
#  - https://docs.mongodb.com/v4.4/reference/configuration-options/#mongodb-setting-systemLog.verbosity
#  - https://docs.mongodb.com/v4.4/reference/log-messages/#std-label-log-messages-configure-verbosity
mongo admin \
  --host localhost \
  --username ${MONGO_INITDB_ROOT_USERNAME} \
  --password ${MONGO_INITDB_ROOT_PASSWORD} \
  --eval "
db.adminCommand( {
   setParameter: 1,
   logComponentVerbosity: {
      verbosity: 0,
      accessControl: {
        verbosity: 1
      },
      command: {
        verbosity: 0
      },
      index: {
        verbosity: 0
      },
      query: {
         verbosity: 0
      },
      storage: {
         verbosity: 1,
         journal: {
            verbosity: 0
         }
      },
      sharding: {
        verbosity: 0
      },
      network: {
        verbosity: 0
      },
      replication: {
        verbosity: 0
      }
   }
} );"

echo "Creating MongoDB user 'coremedia' ..."

mongo admin \
  --host localhost \
  --username ${MONGO_INITDB_ROOT_USERNAME} \
  --password ${MONGO_INITDB_ROOT_PASSWORD} \
  --eval "db.createUser(
    {
      user: \"coremedia\",
      pwd: \"${MONGO_INITDB_COREMEDIA_PASSWORD}\",
      roles: [
        { db: \"coremedia\", role: \"dbOwner\" },
        { db: \"admin\" , role: \"readWriteAnyDatabase\" },
        { db: \"admin\" , role: \"dbAdminAnyDatabase\" }
      ]
    }
  );"

mongo admin \
  --host localhost \
  --username ${MONGO_INITDB_ROOT_USERNAME} \
  --password ${MONGO_INITDB_ROOT_PASSWORD} \
  --eval "db.createUser(
    {
      user: \"monitoring\",
      pwd: \"monitoring\",
      roles: [
        { db: \"monitoring\" , role: \"readWrite\" },
        { db: \"monitoring\" , role: \"dbAdmin\" },
        { db: \"test\" , role: \"readWrite\" },
        { db: \"test\" , role: \"dbAdmin\" },
        { db: \"admin\", role: \"read\" },
        { db: \"admin\", role: \"backup\" },
        { db: \"admin\", role: \"clusterManager\" }
      ]
    }
  );"

echo "MongoDB user created."

mongo admin \
  --host localhost \
  --username ${MONGO_INITDB_ROOT_USERNAME} \
  --password ${MONGO_INITDB_ROOT_PASSWORD} \
  --eval "db.getUser(\"coremedia\");"

mongo admin \
  --host localhost \
  --username ${MONGO_INITDB_ROOT_USERNAME} \
  --password ${MONGO_INITDB_ROOT_PASSWORD} \
  --eval "db.getUser(\"monitoring\");"

echo ""
