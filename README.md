# customized mongodb Container

**Please note:**
The original container is based on [Ubuntu](https://github.com/docker-library/mongo/blob/master/4.4/Dockerfile#L7).
This may lead to corresponding problems regarding updates.


## customizations

- add [`entrypoint.sh`](rootfs/initdb.d/entrypoint.sh) to create custom User
- healthcheck and
- `mongodb exporter` [entrypoint](rootfs/exporter-entrypoint.sh)

## MongoDB version

- `4.4.x`


## Environmentvariables

| name                              | default     |
| :---                              | :---        |
| `MONGO_INITDB_ROOT_USERNAME`      | `root`      |
| `MONGO_INITDB_ROOT_PASSWORD`      | `root`      |
| `MONGO_INITDB_COREMEDIA_USERNAME` | `coremedia` |
| `MONGO_INITDB_COREMEDIA_PASSWORD` | `coremedia` |

## Build

A build can be called via the Makefile.
The following parameters are supported:

- `build`
- `shell`
- `run`
- `exec`
- `start`
- `stop`
- `clean`
- `linter`

### example

```bash
make build
```
