#!/usr/bin/env bash

set -eu

HADOLINT_VERSION='2.6.0'
HADOLINT_PATH="/tmp/hadolint"

if [ ! -x "${HADOLINT_PATH}_${HADOLINT_VERSION}" ]
then
  curl \
    --silent \
    --location \
    --output "${HADOLINT_PATH}_${HADOLINT_VERSION}" \
    "https://github.com/hadolint/hadolint/releases/download/v${HADOLINT_VERSION}/hadolint-Linux-x86_64"
  chmod +x "${HADOLINT_PATH}_${HADOLINT_VERSION}"
fi

${HADOLINT_PATH}_${HADOLINT_VERSION} \
  --ignore DL3013 \
  --ignore DL3018 \
  --ignore SC1091 \
  Dockerfile
