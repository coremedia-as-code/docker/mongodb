# ---------------------------------------------------------------------------------------
#
# downloads

ARG MONGODB_VERSION=4.4

FROM alpine:3 as stage1
ARG MONGODB_EXPORTER=0.30.0

# hadolint ignore=DL3017,DL3018,DL3019
RUN \
  apk update  --quiet

# hadolint ignore=DL3018,DL3019
RUN \
  apk add --quiet   \
    curl

WORKDIR /tmp/

RUN \
  curl \
    --silent \
    --location \
    --retry 3 \
    --output mongodb_exporter-${MONGODB_EXPORTER}.tar.gz \
    https://github.com/percona/mongodb_exporter/releases/download/v${MONGODB_EXPORTER}/mongodb_exporter-${MONGODB_EXPORTER}.linux-amd64.tar.gz

RUN \
  tar -x -a -f mongodb_exporter-${MONGODB_EXPORTER}.tar.gz

RUN \
  mv \
    mongodb_exporter-${MONGODB_EXPORTER}.linux-amd64/mongodb_exporter \
    .

# ---------------------------------------------------------------------------------------

FROM mongo:${MONGODB_VERSION}

ARG BUILD_DATE
ARG BUILD_VERSION
ARG VCS_REF

ENV TERM=xterm

LABEL \
  org.version=${BUILD_VERSION} \
  org.build-date=${BUILD_DATE} \
  org.vcs-ref=${VCS_REF}

RUN apt update && \
    apt \
      --assume-yes \
        dist-upgrade && \
    apt \
      --assume-yes \
        clean && \
    apt \
      --assume-yes \
        autoremove  && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY \
  --from=stage1 \
    /tmp/mongodb_exporter \
    /usr/bin/

COPY \
  rootfs/exporter-entrypoint.sh \
  /usr/local/bin/exporter-entrypoint.sh

COPY \
  --chown=mongodb:mongodb \
  rootfs/initdb.d/* \
  /docker-entrypoint-initdb.d/

ENV MONGO_INITDB_ROOT_USERNAME=root
ENV MONGO_INITDB_ROOT_PASSWORD=root
ENV MONGO_INITDB_COREMEDIA_USERNAME=coremedia
ENV MONGO_INITDB_COREMEDIA_PASSWORD=coremedia

ENTRYPOINT ["exporter-entrypoint.sh"]

CMD ["mongod", "--wiredTigerCacheSizeGB=1"]

EXPOSE 9216

HEALTHCHECK \
  --start-period=30s \
  --interval=30s \
  --retries=2 \
  --timeout=3s \
  CMD mongo --quiet \
    "$(hostname --ip-address || echo '127.0.0.1')/test" \
    --eval 'quit(db.runCommand({ ping: 1 }).ok ? 0 : 2)' || exit 1
